## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.


### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Nokia Vitalqip. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Nokia Vitalqip.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Nokia VitalQIP. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getV4Network(callback)</td>
    <td style="padding:15px">getV4Network</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v4network/{pathv2}{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4Network(callback)</td>
    <td style="padding:15px">deleteV4Network</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v4network/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addV4Network(callback)</td>
    <td style="padding:15px">addV4Network</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v4network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchV4Network(callback)</td>
    <td style="padding:15px">searchV4Network</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v4network{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV6SubnetByAddress(callback)</td>
    <td style="padding:15px">getV6SubnetByAddress</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6subnet/{pathv2}/{pathv3}{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV6SubnetByAddress(callback)</td>
    <td style="padding:15px">deleteV6SubnetByAddress</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6subnet/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV6AddressInV6SubnetByAddress(callback)</td>
    <td style="padding:15px">getV6AddressInV6SubnetByAddress</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6subnet/{pathv2}/{pathv3}/v6addresses{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getListRangeInV6SubnetByAddress(callback)</td>
    <td style="padding:15px">getListRangeInV6SubnetByAddress</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6subnet/{pathv2}/{pathv3}/ranges{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV6SubnetByName(callback)</td>
    <td style="padding:15px">getV6SubnetByName</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6subnet/{pathv2}{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV6SubnetByName(callback)</td>
    <td style="padding:15px">deleteV6SubnetByName</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6subnet/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addV6Subnet(callback)</td>
    <td style="padding:15px">addV6Subnet</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6subnet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateV6Subnet(callback)</td>
    <td style="padding:15px">updateV6Subnet</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6subnet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchV6Subnet(callback)</td>
    <td style="padding:15px">searchV6Subnet</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6subnet{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addV6Address(callback)</td>
    <td style="padding:15px">addV6Address</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateV6Address(callback)</td>
    <td style="padding:15px">updateV6Address</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV6Address(callback)</td>
    <td style="padding:15px">deleteV6Address</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6address/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV6Address(callback)</td>
    <td style="padding:15px">getV6Address</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6address/{pathv2}{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addV4Subnet(callback)</td>
    <td style="padding:15px">addV4Subnet</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v4subnet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateV4Subnet(callback)</td>
    <td style="padding:15px">updateV4Subnet</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v4subnet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchV4Subnet(callback)</td>
    <td style="padding:15px">searchV4Subnet</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v4subnet{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4Subnet(callback)</td>
    <td style="padding:15px">getV4Subnet</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v4subnet/{pathv2}{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4SubnetByName(callback)</td>
    <td style="padding:15px">deleteV4SubnetByName</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v4subnet/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getListAddressV4Subnet(callback)</td>
    <td style="padding:15px">getListAddressV4Subnet</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v4subnet/{pathv2}/v4addresses{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addV4Address(callback)</td>
    <td style="padding:15px">addV4Address</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v4address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateV4Address(callback)</td>
    <td style="padding:15px">updateV4Address</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v4address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4Address(callback)</td>
    <td style="padding:15px">deleteV4Address</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v4address/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4Address(callback)</td>
    <td style="padding:15px">getV4Address</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v4address/{pathv2}{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteASingleRr(callback)</td>
    <td style="padding:15px">deleteASingleRr</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/rr/singleDelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRRbyInfraTypebyNameOrAddress(callback)</td>
    <td style="padding:15px">deleteRRbyInfraTypebyNameOrAddress</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/rr/{pathv2}/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRRsbyNameOrAddress(callback)</td>
    <td style="padding:15px">deleteRRsbyNameOrAddress</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/rr/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRRByName(callback)</td>
    <td style="padding:15px">getRRByName</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/rr{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addRR(callback)</td>
    <td style="padding:15px">addRR</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/rr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRR(callback)</td>
    <td style="padding:15px">DeleteRR</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/rr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRR(callback)</td>
    <td style="padding:15px">updateRR</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/rr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSelectedV4address(callback)</td>
    <td style="padding:15px">DeleteSelectedV4address</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/selectedv4address/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">selectedV4addressWithRange(callback)</td>
    <td style="padding:15px">selectedV4addressWithRange</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/selectedv4address/{pathv2}{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSelectedV6address(callback)</td>
    <td style="padding:15px">DeleteSelectedV6address</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/selectedv6address/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">selectedV6addressWithRange(callback)</td>
    <td style="padding:15px">selectedV6addressWithRange</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/selectedv6address/{pathv2}{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addZone(callback)</td>
    <td style="padding:15px">addZone</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/zone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyZone(callback)</td>
    <td style="padding:15px">modifyZone</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/zone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchZone(callback)</td>
    <td style="padding:15px">searchZone</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/zone{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZone(callback)</td>
    <td style="padding:15px">getZone</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/zone/{pathv2}{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteZone(callback)</td>
    <td style="padding:15px">deleteZone</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/zone/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addV6BlockToPool(callback)</td>
    <td style="padding:15px">addV6BlockToPool</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6block?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyV6Block(callback)</td>
    <td style="padding:15px">modifyV6Block</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6block?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchV6Block(callback)</td>
    <td style="padding:15px">searchV6Block</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6block{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV6BlockByUUID(callback)</td>
    <td style="padding:15px">getV6BlockByUUID</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6block/{pathv2}{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV6BlockByBlockInfo(callback)</td>
    <td style="padding:15px">deleteV6BlockByBlockInfo</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6block/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV6BlockByAddress(callback)</td>
    <td style="padding:15px">deleteV6BlockByAddress</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6block/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignV6Block(callback)</td>
    <td style="padding:15px">assignV6Block</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6block/assignment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchV6Range(callback)</td>
    <td style="padding:15px">searchV6Range</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6range{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addV6Range(callback)</td>
    <td style="padding:15px">addV6Range</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV6Range(callback)</td>
    <td style="padding:15px">deleteV6Range</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyV6Range(callback)</td>
    <td style="padding:15px">modifyV6Range</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchV6Pool(callback)</td>
    <td style="padding:15px">searchV6Pool</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6pool{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV6PoolByUUID(callback)</td>
    <td style="padding:15px">getV6PoolByUUID</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6pool/{pathv2}{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addV6Pool(callback)</td>
    <td style="padding:15px">addV6Pool</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6pool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyV6Pool(callback)</td>
    <td style="padding:15px">modifyV6Pool</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6pool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV6Pool(callback)</td>
    <td style="padding:15px">deleteV6Pool</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6pool/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyV4Network(orgName, requestBody, callback)</td>
    <td style="padding:15px">Update V4 network</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v4network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">splitV4Subnet(orgName, requestBody, callback)</td>
    <td style="padding:15px">Split V4 Subnet</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v4subnet/split?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">joinV4Subnet(orgName, requestBody, callback)</td>
    <td style="padding:15px">Join V4 Subnet</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v4subnet/join?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateObjectName(orgName, objectClass, callback)</td>
    <td style="padding:15px">Generate hostname automatically</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v4address/generateobjectname?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateV4Addresses(orgName, requestBody, callback)</td>
    <td style="padding:15px">Update V4 Addresses</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v4addresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchV4Address(orgName, address, name, udaName, udaValue, pageSize, pageIndex, extension = '.json', callback)</td>
    <td style="padding:15px">Search V4 Address</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v4address{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteARrByRrId(orgName, rrId, callback)</td>
    <td style="padding:15px">Delete RR</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/rr/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">moveV4Object(orgName, requestBody, callback)</td>
    <td style="padding:15px">Move an object by IP address.</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/moveV4Object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getipv4address(orgName, subnetAddress, startExcludedAddress, excludedAddressCount, extension = '.json', callback)</td>
    <td style="padding:15px">Select an IPv4 Address from a specific IPv4 Subnet.</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/address/getipv4address{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV1OrgNameV4addressGenerateauditreport(orgName, startDate, endDate, name, address, udaName, udaValue, reportFormat = 'html', callback)</td>
    <td style="padding:15px">Download audit report file</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v4address/generateauditreport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getfreesubnetlist(orgName, networkAddress, subnetMask, type = 'New', startAddress, numberOfSubnet, extension = '.json', callback)</td>
    <td style="padding:15px">Get all available existing or new subnets in the IPv4 Network</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v4network/getfreesubnetlist{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">movev6object(orgName, requestBody, callback)</td>
    <td style="padding:15px">Move one or more object(s) by IPv6 address.</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/v6address/movev6object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addForwardedZone(orgName, requestBody, callback)</td>
    <td style="padding:15px">Create forwarded zone</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/zone/forwardedzone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyForwardedZone(orgName, forwardedZoneName, requestBody, callback)</td>
    <td style="padding:15px">Modify a forwarded zone</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/zone/forwardedzone/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteForwardedZone(orgName, forwardedZoneName, callback)</td>
    <td style="padding:15px">Delete a forwarded zone.</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/zone/forwardedzone/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForwardedZone(orgName, extension = '.json', forwardedZoneName, callback)</td>
    <td style="padding:15px">Get Forwarded Zone</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/zone/forwardedzone/{pathv2}{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">signzone(orgName, requestBody, callback)</td>
    <td style="padding:15px">Sign a Zone</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/zone/signzone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unsignzone(orgName, zoneName, emergency = 'true', callback)</td>
    <td style="padding:15px">Unsign a Zone</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/zone/unsignzone/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pushDNS(orgName, requestBody, extension = '.json', callback)</td>
    <td style="padding:15px">Generate DNS Server Configuration</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dns/generation{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSServer(orgName, fqdn, extension = '.json', callback)</td>
    <td style="padding:15px">Get DNS Server by FQDN</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dns/{pathv2}{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchDNSServer(orgName, name, address, pageSize, pageIndex, extension = '.json', callback)</td>
    <td style="padding:15px">Search DNS Server by Name or Address</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dns{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDNS(orgName, requestBody, callback)</td>
    <td style="padding:15px">Create a DNS Server</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSServer(orgName, serverFQDN, callback)</td>
    <td style="padding:15px">Delete DNS Server</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dns/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyDNS(orgName, serverFQDN, requestBody, callback)</td>
    <td style="padding:15px">Modify a DNS Server</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dns/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSServerZone(orgName, fqdn, zoneName, pageSize, pageIndex, sort, extension = '.json', callback)</td>
    <td style="padding:15px">Get DNS Server Zones</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dns/serverZone{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">managezone(orgName, fqdn, requestBody, callback)</td>
    <td style="padding:15px">Manage/remove zones from the DNS Server.</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dns/{pathv2}/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsServerTemplate(extension = '.json', orgName, name, callback)</td>
    <td style="padding:15px">Get DNS Server Template</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dnsservertemplate/{pathv2}{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnsServerTemplate(orgName, name, callback)</td>
    <td style="padding:15px">Delete DNS Server Template</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dnsservertemplate/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDNSServerTemplate(orgName, requestBody, callback)</td>
    <td style="padding:15px">Create DNS Server Template</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dnsservertemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyDnsServerTemplate(orgName, name, requestBody, callback)</td>
    <td style="padding:15px">Modify DNS Server Template</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dnsservertemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchDnsServerTemplate(extension = '.json', orgName, name, type, zone, pageSize, pageIndex, callback)</td>
    <td style="padding:15px">Search DNS Server Template</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dnsservertemplate{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateDHCP(orgName, requestBody, extension = '.json', callback)</td>
    <td style="padding:15px">Generate DHCP Server Configuration</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dhcp/generation{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createScope(orgName, requestBody, callback)</td>
    <td style="padding:15px">Create a DHCP scope by selecting a set of addresses and assigning it an Object Profile</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dhcp/scope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyScope(orgName, startAddress, endAddress, requestBody, callback)</td>
    <td style="padding:15px">Modify a DHCP scope</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dhcp/scope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDHCP(orgName, requestBody, callback)</td>
    <td style="padding:15px">Create a DHCP Server</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dhcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDHCPServer(orgName, serverFQDN, callback)</td>
    <td style="padding:15px">Delete DHCP Server</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dhcp/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyDHCP(orgName, serverFQDN, requestBody, callback)</td>
    <td style="padding:15px">Modify a DHCP Server</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dhcp/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchDHCPServer(orgName, name, address, pageSize, pageIndex, extension = '.json', callback)</td>
    <td style="padding:15px">Search DHCP Server by Name or Address</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dhcp{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPServer(orgName, fqdn, extension = '.json', callback)</td>
    <td style="padding:15px">Get DHCP Server by FQDN</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dhcp/{pathv2}{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">explicitBlock(orgName, requestBody, callback)</td>
    <td style="padding:15px">Explicit block and send arin notify</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/block/explicit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">allocateBlock(orgName, requestBody, callback)</td>
    <td style="padding:15px">Allocate block and send arin notify</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/block/allocate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">recoverBlock(orgName, requestBody, callback)</td>
    <td style="padding:15px">Recover block and send arin notify</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/block/recover?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">quickBlock(orgName, requestBody, callback)</td>
    <td style="padding:15px">Quick block and send arin notify</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/block/quick?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkStatus(orgName, token, extension = '.json', callback)</td>
    <td style="padding:15px">Check status of a job</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/jobstatus{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadConfigFiles(orgName, token, files, type = 'zip', callback)</td>
    <td style="padding:15px">Download push configuration files</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/download/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">decodeX509Certificate(certificate, callback)</td>
    <td style="padding:15px">Decode encoded certificate</td>
    <td style="padding:15px">{base_path}/{version}/global/v1/utils/x509/decode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateKeyPair(requestBody, callback)</td>
    <td style="padding:15px">Generate certificate key pair (self-signed)</td>
    <td style="padding:15px">{base_path}/{version}/global/v1/utils/x509/generate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSamlIdp(requestBody, callback)</td>
    <td style="padding:15px">Create SAML Identity Provider</td>
    <td style="padding:15px">{base_path}/{version}/global/v1/samlidp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifySamlIdentityProvider(requestBody, callback)</td>
    <td style="padding:15px">Modify SAML Identity Provider</td>
    <td style="padding:15px">{base_path}/{version}/global/v1/samlidp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getByName(extension = '.json', samlIdpName, callback)</td>
    <td style="padding:15px">Get SAML Identity Provider by name</td>
    <td style="padding:15px">{base_path}/{version}/global/v1/samlidp/{pathv1}{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchSamlIdpByName(extension = '.json', samlIdpName, pageIndex, pageSize, callback)</td>
    <td style="padding:15px">Search SAML Identity Providers by name</td>
    <td style="padding:15px">{base_path}/{version}/global/v1/samlidp{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSAMLIdentityProvider(samlIdpName, callback)</td>
    <td style="padding:15px">Delete SAML Identity Provider</td>
    <td style="padding:15px">{base_path}/{version}/global/v1/samlidp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSamlConfiguration(requestBody, callback)</td>
    <td style="padding:15px">Update SAML Configuration</td>
    <td style="padding:15px">{base_path}/{version}/global/v1/samlconfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSamlConfiguration(extension = '.json', callback)</td>
    <td style="padding:15px">Get SAML Configuration</td>
    <td style="padding:15px">{base_path}/{version}/global/v1/samlconfiguration{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRunningSamlConfiguration(extension = '.json', callback)</td>
    <td style="padding:15px">Get running configuration</td>
    <td style="padding:15px">{base_path}/{version}/global/v1/samlconfiguration/running{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">qipSearch(orgName, name, address, searchType, subSearchType, subRange, target, extension = '.json', callback)</td>
    <td style="padding:15px">qip-search</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/qip-search{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV1OrgNameManagedTsigkeyExtension(orgName, extension = '.json', callback)</td>
    <td style="padding:15px">Get managed TSIG keys used to secure VitalQIP TSIG DNS updates</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/managed-tsigkey{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV1OrgNameManagedTsigkey(orgName, body, callback)</td>
    <td style="padding:15px">Initiate a manual managed TSIG key rollover</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/managed-tsigkey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdmin(orgName, loginName, extension = '.json', callback)</td>
    <td style="padding:15px">Get an administrator account.</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/admin/{pathv2}{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchAdmin(orgName, extension = '.json', loginName, adminType = 'All', adminStatus = 'All', pageIndex, pageSize, callback)</td>
    <td style="padding:15px">Search administrator.</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/admin{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAdmin(orgName, requestBody, callback)</td>
    <td style="padding:15px">Create an administrator account.</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/admin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyAdmin(orgName, loginName, requestBody, callback)</td>
    <td style="padding:15px">Modify an administrator account.</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/admin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAdmin(orgName, loginName, callback)</td>
    <td style="padding:15px">Delete an administrator account.</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/admin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changePassword(orgName, loginName, newPassword, confirmNewPassword, callback)</td>
    <td style="padding:15px">Change password for an administrator account.</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/admin/changePassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getManagedList(orgName, loginName, assignedOrgName, extension = '.json', callback)</td>
    <td style="padding:15px">Get the managed list of an administrator by login name and organization.</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/admin/managedList{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignManageList(orgName, loginName, assignedOrgName, requestBody, callback)</td>
    <td style="padding:15px">Assign a manage list for an administrator.</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/admin/managedList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReport(orgName, adminNames, adminStatus = 'All', reportFormat = 'html', callback)</td>
    <td style="padding:15px">Download Administrators report file</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/admin/report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activityreport(orgName, administrator, fromDate, toDate, sortOrder = 'Ascending', reportFormat = 'html', callback)</td>
    <td style="padding:15px">Download Administrators Activity report file in ALL organization</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/admin/activityreport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAdminRole(orgName, requestBody, callback)</td>
    <td style="padding:15px">Create an administrator role.</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/admin/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchAdminRole(orgName, extension = '.json', adminRoleName, pageIndex, pageSize, callback)</td>
    <td style="padding:15px">Search administrator role.</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/admin/role{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyAdminRole(orgName, adminRoleName, requestBody, callback)</td>
    <td style="padding:15px">Modify an administrator role.</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/admin/role/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAdminRole(orgName, adminRoleName, callback)</td>
    <td style="padding:15px">Delete an administrator role.</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/admin/role/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchAdminRoleAssignedAdmin(orgName, extension = '.json', adminRoleName, pageIndex, pageSize, callback)</td>
    <td style="padding:15px">Get assigned administrator name list by administrator role.</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/admin/role/{pathv2}/assignedAdmin{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdminRoleManagedList(orgName, extension = '.json', adminRoleName, callback)</td>
    <td style="padding:15px">Get the managed list of an administrator role by role name.</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/admin/role/{pathv2}/managedList{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignAdminRoleManagedList(orgName, adminRoleName, requestBody, callback)</td>
    <td style="padding:15px">Assign a managed list for an administrator role.</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/admin/role/{pathv2}/managedList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDHCPTemplate(orgName, requestBody, callback)</td>
    <td style="padding:15px">Create a DHCP Option Template</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dhcpoptiontemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyDHCPOptonTemplate(orgName, templateName, requestBody, callback)</td>
    <td style="padding:15px">Modify a DHCP Option Template.</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dhcpoptiontemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDhcpOptionTemplate(orgName, templateName, callback)</td>
    <td style="padding:15px">Delete a DHCP Option Template</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dhcpoptiontemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPOptionTemplate(orgName, extension = '.json', address, templateName, infraType = 'OBJECT', callback)</td>
    <td style="padding:15px">Get DHCP Options Template by Subnet/Object/DHCP Option Template</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dhcpoptiontemplate/{pathv2}{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchDhcpOptionTemplateOptNum(orgName, extension = '.json', optionNumber, pageIndex, pageSize, callback)</td>
    <td style="padding:15px">Search DHCP Option Template by Option Number</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dhcpoptiontemplate/searchoptiontmplbyoptnum{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchDhcpOptionTemplateName(orgName, extension = '.json', templateName, startIndex, numberElement, callback)</td>
    <td style="padding:15px">Search DHCP Option Template by name</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dhcpoptiontemplate/searchoptiontmplbyname{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpPolicyTemplate(orgName, extension = '.json', policyTemplateName, callback)</td>
    <td style="padding:15px">Get DHCP Policy Template by Name</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dhcppolicytemplate/{pathv2}{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDhcpPolicyTemplate(orgName, requestBody, callback)</td>
    <td style="padding:15px">Create a DHCP Policy Template</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dhcppolicytemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyDHCPolicyTemplate(orgName, templateName, requestBody, callback)</td>
    <td style="padding:15px">Modify a DHCP Policy Template.</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dhcppolicytemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDHCPPolicyTemplate(orgName, templateName, callback)</td>
    <td style="padding:15px">Delete a DHCP Policy Template</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/dhcppolicytemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV1OrgNameNode(orgName, requestBody, callback)</td>
    <td style="padding:15px">Create Node</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/node?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV1OrgNameNode(orgName, uniqueID, requestBody, callback)</td>
    <td style="padding:15px">Modify Node</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/node?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV1OrgNameNodeExtension(orgName, extension = '.json', nodeName, uniqueID, duid, addressName, address, addressType = 'V4', pageIndex, pageSize, callback)</td>
    <td style="padding:15px">Search node</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/node{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV1OrgNameNodeOptionsExtension(orgName, extension = '.json', options = 'uniqueID', nodeInfo, addressType = 'V6', callback)</td>
    <td style="padding:15px">Get Node</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/node/{pathv2}{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV1OrgNameNodeUniqueID(orgName, uniqueID, callback)</td>
    <td style="padding:15px">Delete Node</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/node/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV1OrgNameNodeLinknodetoipaddresses(orgName, requestBody, callback)</td>
    <td style="padding:15px">Link Node to IP addresses</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/node/linknodetoipaddresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV1OrgNameNodeUnlinkipv4addressesfromnodes(orgName, requestBody, callback)</td>
    <td style="padding:15px">unLink IPv4 addresses from Node</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/node/unlinkipv4addressesfromnodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSubnetOrganization(orgName, requestBody, callback)</td>
    <td style="padding:15px">Create a subnet organization</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/subnetorganization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetOrg(orgName, extension = '.json', subnetOrgName, callback)</td>
    <td style="padding:15px">Get Subnet Organization</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/subnetorganization/{pathv2}{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pageSearch(orgName, subnetOrgName, dhcpServerName, dhcpOptionTemplateName, siteName, udaName, udaValue, pageIndex, pageSize, extension = '.json', callback)</td>
    <td style="padding:15px">Search subnet organization</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/subnetorganization{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifySubnetOrg(orgName, subnetOrgName, requestBody, callback)</td>
    <td style="padding:15px">Modify a subnet organization</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/subnetorganization/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubnetOrg(orgName, subnetOrgName, callback)</td>
    <td style="padding:15px">Delete a subnet organization</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/subnetorganization/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDHCPAndV4SubnetMacPool(orgName, infraValue, infraType = 'DHCP Server', requestBody, callback)</td>
    <td style="padding:15px">Add a MAC to a DHCP Server or V4 Subnet MAC Pool</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/macpool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyDHCPAndV4SubnetMacPool(orgName, infraValue, infraType = 'DHCP Server', requestBody, callback)</td>
    <td style="padding:15px">Modify MAC to a DHCP Server or V4 Subnet MAC Pool</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/macpool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDHCPAndV4SubnetMacPool(orgName, infraValue, infraType = 'DHCP Server', requestBody, callback)</td>
    <td style="padding:15px">Delete MAC for a DHCP Server or V4 Subnet MAC Pool</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/macpool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPAndV4SubnetMacPool(orgName, extension = '.json', infraValue, infraType = 'DHCP Server', callback)</td>
    <td style="padding:15px">Get list MAC for a DHCP Server or V4 Subnet MAC Pool</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/macpool/{pathv2}{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addACLTemplate(orgName, requestBody, callback)</td>
    <td style="padding:15px">Create ACL Template</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/acltemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getACLTemplate(orgName, aclTemplateName, extension = '.json', callback)</td>
    <td style="padding:15px">Get ACL Template</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/acltemplate/{pathv2}{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyAclTemplate(orgName, aclTemplateName, requestBody, callback)</td>
    <td style="padding:15px">Modify ACL Template</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/acltemplate/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAclTemplate(orgName, aclTemplateName, force = 'false', callback)</td>
    <td style="padding:15px">Delete ACL Template</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/acltemplate/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchACLTemplate(orgName, aclTemplateName, templateElementType = 'ACL Template', templateElementValue, pageIndex, pageSize, extension = '.json', callback)</td>
    <td style="padding:15px">Search ACL template</td>
    <td style="padding:15px">{base_path}/{version}/v1/{pathv1}/acltemplate{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
