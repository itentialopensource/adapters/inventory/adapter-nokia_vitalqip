
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:17PM

See merge request itentialopensource/adapters/adapter-nokia_vitalqip!13

---

## 0.4.3 [09-13-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-nokia_vitalqip!11

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:28PM

See merge request itentialopensource/adapters/adapter-nokia_vitalqip!10

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:40PM

See merge request itentialopensource/adapters/adapter-nokia_vitalqip!9

---

## 0.4.0 [05-09-2024]

* 2024 Adapter Migration

See merge request itentialopensource/adapters/inventory/adapter-nokia_vitalqip!8

---

## 0.3.4 [03-26-2024]

* Changes made at 2024.03.26_14:43PM

See merge request itentialopensource/adapters/inventory/adapter-nokia_vitalqip!7

---

## 0.3.3 [03-13-2024]

* Changes made at 2024.03.13_14:01PM

See merge request itentialopensource/adapters/inventory/adapter-nokia_vitalqip!6

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_14:10PM

See merge request itentialopensource/adapters/inventory/adapter-nokia_vitalqip!5

---

## 0.3.1 [02-26-2024]

* Changes made at 2024.02.26_13:38PM

See merge request itentialopensource/adapters/inventory/adapter-nokia_vitalqip!4

---

## 0.3.0 [12-29-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/inventory/adapter-nokia_vitalqip!2

---

## 0.2.1 [09-29-2023]

* Add new calls

See merge request itentialopensource/adapters/inventory/adapter-nokia_vitalqip!3

---

## 0.2.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/inventory/adapter-nokia_vitalqip!1

---

## 0.1.1 [10-26-2021]

- Initial Commit

See commit e1fabeb

---
