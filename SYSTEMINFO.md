# Nokia VitalQIP

Vendor: Nokia VitalQIP
Homepage: [https://Nokia VitalQIP.com/](https://www.nokia.com/)

Product: Nokia VitalQIP
Product Page: [https://Nokia VitalQIP.com/](https://www.nokia.com/networks/bss-oss/vitalqip-ip-address-management/)

## Introduction
We classify Nokia Vitalqip into the Inventory and Network Services domain since Nokia Vitalqip is a robust and scalable DNS/DHCP/IP Address Management (DDI) solution designed to efficiently manage large-scale IP networks.

## Why Integrate
The Nokia Vitalqip adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Nokia Vitalqip. With this adapter you have the ability to perform operations such as:

- IPAM
- DHCP
- DNS

## Additional Product Documentation