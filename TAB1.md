# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Nokia_vitalqip System. The API that was used to build the adapter for Nokia_vitalqip is usually available in the report directory of this adapter. The adapter utilizes the Nokia_vitalqip API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Nokia Vitalqip adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Nokia Vitalqip. With this adapter you have the ability to perform operations such as:

- IPAM
- DHCP
- DNS

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
