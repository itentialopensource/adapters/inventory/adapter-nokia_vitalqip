
## 0.2.1 [09-29-2023]

* Add new calls

See merge request itentialopensource/adapters/inventory/adapter-nokia_vitalqip!3

---

## 0.2.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/inventory/adapter-nokia_vitalqip!1

---

## 0.1.1 [10-26-2021]

- Initial Commit

See commit e1fabeb

---
