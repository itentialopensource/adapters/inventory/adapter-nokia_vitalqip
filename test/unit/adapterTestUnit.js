/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-nokia_vitalqip',
      type: 'NokiaVitalqip',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const NokiaVitalqip = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Nokia_vitalqip Adapter Test', () => {
  describe('NokiaVitalqip Class Tests', () => {
    const a = new NokiaVitalqip(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('nokia_vitalqip'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('nokia_vitalqip'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('NokiaVitalqip', pronghornDotJson.export);
          assert.equal('Nokia_vitalqip', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-nokia_vitalqip', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('nokia_vitalqip'));
          assert.equal('NokiaVitalqip', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-nokia_vitalqip', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-nokia_vitalqip', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getV4Network - errors', () => {
      it('should have a getV4Network function', (done) => {
        try {
          assert.equal(true, typeof a.getV4Network === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4Network - errors', () => {
      it('should have a deleteV4Network function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4Network === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addV4Network - errors', () => {
      it('should have a addV4Network function', (done) => {
        try {
          assert.equal(true, typeof a.addV4Network === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchV4Network - errors', () => {
      it('should have a searchV4Network function', (done) => {
        try {
          assert.equal(true, typeof a.searchV4Network === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV6SubnetByAddress - errors', () => {
      it('should have a getV6SubnetByAddress function', (done) => {
        try {
          assert.equal(true, typeof a.getV6SubnetByAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV6SubnetByAddress - errors', () => {
      it('should have a deleteV6SubnetByAddress function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV6SubnetByAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV6AddressInV6SubnetByAddress - errors', () => {
      it('should have a getV6AddressInV6SubnetByAddress function', (done) => {
        try {
          assert.equal(true, typeof a.getV6AddressInV6SubnetByAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getListRangeInV6SubnetByAddress - errors', () => {
      it('should have a getListRangeInV6SubnetByAddress function', (done) => {
        try {
          assert.equal(true, typeof a.getListRangeInV6SubnetByAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV6SubnetByName - errors', () => {
      it('should have a getV6SubnetByName function', (done) => {
        try {
          assert.equal(true, typeof a.getV6SubnetByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV6SubnetByName - errors', () => {
      it('should have a deleteV6SubnetByName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV6SubnetByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addV6Subnet - errors', () => {
      it('should have a addV6Subnet function', (done) => {
        try {
          assert.equal(true, typeof a.addV6Subnet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateV6Subnet - errors', () => {
      it('should have a updateV6Subnet function', (done) => {
        try {
          assert.equal(true, typeof a.updateV6Subnet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchV6Subnet - errors', () => {
      it('should have a searchV6Subnet function', (done) => {
        try {
          assert.equal(true, typeof a.searchV6Subnet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addV6Address - errors', () => {
      it('should have a addV6Address function', (done) => {
        try {
          assert.equal(true, typeof a.addV6Address === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateV6Address - errors', () => {
      it('should have a updateV6Address function', (done) => {
        try {
          assert.equal(true, typeof a.updateV6Address === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV6Address - errors', () => {
      it('should have a deleteV6Address function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV6Address === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV6Address - errors', () => {
      it('should have a getV6Address function', (done) => {
        try {
          assert.equal(true, typeof a.getV6Address === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addV4Subnet - errors', () => {
      it('should have a addV4Subnet function', (done) => {
        try {
          assert.equal(true, typeof a.addV4Subnet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateV4Subnet - errors', () => {
      it('should have a updateV4Subnet function', (done) => {
        try {
          assert.equal(true, typeof a.updateV4Subnet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchV4Subnet - errors', () => {
      it('should have a searchV4Subnet function', (done) => {
        try {
          assert.equal(true, typeof a.searchV4Subnet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Subnet - errors', () => {
      it('should have a getV4Subnet function', (done) => {
        try {
          assert.equal(true, typeof a.getV4Subnet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4SubnetByName - errors', () => {
      it('should have a deleteV4SubnetByName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4SubnetByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getListAddressV4Subnet - errors', () => {
      it('should have a getListAddressV4Subnet function', (done) => {
        try {
          assert.equal(true, typeof a.getListAddressV4Subnet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addV4Address - errors', () => {
      it('should have a addV4Address function', (done) => {
        try {
          assert.equal(true, typeof a.addV4Address === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateV4Address - errors', () => {
      it('should have a updateV4Address function', (done) => {
        try {
          assert.equal(true, typeof a.updateV4Address === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4Address - errors', () => {
      it('should have a deleteV4Address function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4Address === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Address - errors', () => {
      it('should have a getV4Address function', (done) => {
        try {
          assert.equal(true, typeof a.getV4Address === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteASingleRr - errors', () => {
      it('should have a deleteASingleRr function', (done) => {
        try {
          assert.equal(true, typeof a.deleteASingleRr === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRRbyInfraTypebyNameOrAddress - errors', () => {
      it('should have a deleteRRbyInfraTypebyNameOrAddress function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRRbyInfraTypebyNameOrAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRRsbyNameOrAddress - errors', () => {
      it('should have a deleteRRsbyNameOrAddress function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRRsbyNameOrAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRRByName - errors', () => {
      it('should have a getRRByName function', (done) => {
        try {
          assert.equal(true, typeof a.getRRByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addRR - errors', () => {
      it('should have a addRR function', (done) => {
        try {
          assert.equal(true, typeof a.addRR === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRR - errors', () => {
      it('should have a deleteRR function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRR === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRR - errors', () => {
      it('should have a updateRR function', (done) => {
        try {
          assert.equal(true, typeof a.updateRR === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSelectedV4address - errors', () => {
      it('should have a deleteSelectedV4address function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSelectedV4address === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#selectedV4addressWithRange - errors', () => {
      it('should have a selectedV4addressWithRange function', (done) => {
        try {
          assert.equal(true, typeof a.selectedV4addressWithRange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSelectedV6address - errors', () => {
      it('should have a deleteSelectedV6address function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSelectedV6address === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#selectedV6addressWithRange - errors', () => {
      it('should have a selectedV6addressWithRange function', (done) => {
        try {
          assert.equal(true, typeof a.selectedV6addressWithRange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addZone - errors', () => {
      it('should have a addZone function', (done) => {
        try {
          assert.equal(true, typeof a.addZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyZone - errors', () => {
      it('should have a modifyZone function', (done) => {
        try {
          assert.equal(true, typeof a.modifyZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchZone - errors', () => {
      it('should have a searchZone function', (done) => {
        try {
          assert.equal(true, typeof a.searchZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZone - errors', () => {
      it('should have a getZone function', (done) => {
        try {
          assert.equal(true, typeof a.getZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteZone - errors', () => {
      it('should have a deleteZone function', (done) => {
        try {
          assert.equal(true, typeof a.deleteZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addV6BlockToPool - errors', () => {
      it('should have a addV6BlockToPool function', (done) => {
        try {
          assert.equal(true, typeof a.addV6BlockToPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyV6Block - errors', () => {
      it('should have a modifyV6Block function', (done) => {
        try {
          assert.equal(true, typeof a.modifyV6Block === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchV6Block - errors', () => {
      it('should have a searchV6Block function', (done) => {
        try {
          assert.equal(true, typeof a.searchV6Block === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV6BlockByUUID - errors', () => {
      it('should have a getV6BlockByUUID function', (done) => {
        try {
          assert.equal(true, typeof a.getV6BlockByUUID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV6BlockByBlockInfo - errors', () => {
      it('should have a deleteV6BlockByBlockInfo function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV6BlockByBlockInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV6BlockByAddress - errors', () => {
      it('should have a deleteV6BlockByAddress function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV6BlockByAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignV6Block - errors', () => {
      it('should have a assignV6Block function', (done) => {
        try {
          assert.equal(true, typeof a.assignV6Block === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchV6Range - errors', () => {
      it('should have a searchV6Range function', (done) => {
        try {
          assert.equal(true, typeof a.searchV6Range === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addV6Range - errors', () => {
      it('should have a addV6Range function', (done) => {
        try {
          assert.equal(true, typeof a.addV6Range === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV6Range - errors', () => {
      it('should have a deleteV6Range function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV6Range === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyV6Range - errors', () => {
      it('should have a modifyV6Range function', (done) => {
        try {
          assert.equal(true, typeof a.modifyV6Range === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchV6Pool - errors', () => {
      it('should have a searchV6Pool function', (done) => {
        try {
          assert.equal(true, typeof a.searchV6Pool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV6PoolByUUID - errors', () => {
      it('should have a getV6PoolByUUID function', (done) => {
        try {
          assert.equal(true, typeof a.getV6PoolByUUID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addV6Pool - errors', () => {
      it('should have a addV6Pool function', (done) => {
        try {
          assert.equal(true, typeof a.addV6Pool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyV6Pool - errors', () => {
      it('should have a modifyV6Pool function', (done) => {
        try {
          assert.equal(true, typeof a.modifyV6Pool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV6Pool - errors', () => {
      it('should have a deleteV6Pool function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV6Pool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyV4Network - errors', () => {
      it('should have a modifyV4Network function', (done) => {
        try {
          assert.equal(true, typeof a.modifyV4Network === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.modifyV4Network(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyV4Network', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.modifyV4Network('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyV4Network', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#splitV4Subnet - errors', () => {
      it('should have a splitV4Subnet function', (done) => {
        try {
          assert.equal(true, typeof a.splitV4Subnet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.splitV4Subnet(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-splitV4Subnet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.splitV4Subnet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-splitV4Subnet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#joinV4Subnet - errors', () => {
      it('should have a joinV4Subnet function', (done) => {
        try {
          assert.equal(true, typeof a.joinV4Subnet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.joinV4Subnet(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-joinV4Subnet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.joinV4Subnet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-joinV4Subnet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateObjectName - errors', () => {
      it('should have a generateObjectName function', (done) => {
        try {
          assert.equal(true, typeof a.generateObjectName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.generateObjectName(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-generateObjectName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectClass', (done) => {
        try {
          a.generateObjectName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objectClass is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-generateObjectName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateV4Addresses - errors', () => {
      it('should have a updateV4Addresses function', (done) => {
        try {
          assert.equal(true, typeof a.updateV4Addresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.updateV4Addresses(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-updateV4Addresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.updateV4Addresses('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-updateV4Addresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchV4Address - errors', () => {
      it('should have a searchV4Address function', (done) => {
        try {
          assert.equal(true, typeof a.searchV4Address === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.searchV4Address(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchV4Address', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.searchV4Address('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchV4Address', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteARrByRrId - errors', () => {
      it('should have a deleteARrByRrId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteARrByRrId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.deleteARrByRrId(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteARrByRrId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rrId', (done) => {
        try {
          a.deleteARrByRrId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'rrId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteARrByRrId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#moveV4Object - errors', () => {
      it('should have a moveV4Object function', (done) => {
        try {
          assert.equal(true, typeof a.moveV4Object === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.moveV4Object(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-moveV4Object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.moveV4Object('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-moveV4Object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getipv4address - errors', () => {
      it('should have a getipv4address function', (done) => {
        try {
          assert.equal(true, typeof a.getipv4address === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getipv4address(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getipv4address', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetAddress', (done) => {
        try {
          a.getipv4address('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'subnetAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getipv4address', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.getipv4address('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getipv4address', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV1OrgNameV4addressGenerateauditreport - errors', () => {
      it('should have a getV1OrgNameV4addressGenerateauditreport function', (done) => {
        try {
          assert.equal(true, typeof a.getV1OrgNameV4addressGenerateauditreport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getV1OrgNameV4addressGenerateauditreport(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getV1OrgNameV4addressGenerateauditreport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportFormat', (done) => {
        try {
          a.getV1OrgNameV4addressGenerateauditreport('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'reportFormat is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getV1OrgNameV4addressGenerateauditreport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getfreesubnetlist - errors', () => {
      it('should have a getfreesubnetlist function', (done) => {
        try {
          assert.equal(true, typeof a.getfreesubnetlist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getfreesubnetlist(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getfreesubnetlist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkAddress', (done) => {
        try {
          a.getfreesubnetlist('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getfreesubnetlist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetMask', (done) => {
        try {
          a.getfreesubnetlist('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'subnetMask is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getfreesubnetlist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.getfreesubnetlist('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getfreesubnetlist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#movev6object - errors', () => {
      it('should have a movev6object function', (done) => {
        try {
          assert.equal(true, typeof a.movev6object === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.movev6object(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-movev6object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.movev6object('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-movev6object', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addForwardedZone - errors', () => {
      it('should have a addForwardedZone function', (done) => {
        try {
          assert.equal(true, typeof a.addForwardedZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.addForwardedZone(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-addForwardedZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.addForwardedZone('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-addForwardedZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyForwardedZone - errors', () => {
      it('should have a modifyForwardedZone function', (done) => {
        try {
          assert.equal(true, typeof a.modifyForwardedZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.modifyForwardedZone(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyForwardedZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing forwardedZoneName', (done) => {
        try {
          a.modifyForwardedZone('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'forwardedZoneName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyForwardedZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.modifyForwardedZone('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyForwardedZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteForwardedZone - errors', () => {
      it('should have a deleteForwardedZone function', (done) => {
        try {
          assert.equal(true, typeof a.deleteForwardedZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.deleteForwardedZone(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteForwardedZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing forwardedZoneName', (done) => {
        try {
          a.deleteForwardedZone('fakeparam', null, (data, error) => {
            try {
              const displayE = 'forwardedZoneName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteForwardedZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getForwardedZone - errors', () => {
      it('should have a getForwardedZone function', (done) => {
        try {
          assert.equal(true, typeof a.getForwardedZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getForwardedZone(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getForwardedZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.getForwardedZone('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getForwardedZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing forwardedZoneName', (done) => {
        try {
          a.getForwardedZone('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'forwardedZoneName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getForwardedZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#signzone - errors', () => {
      it('should have a signzone function', (done) => {
        try {
          assert.equal(true, typeof a.signzone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.signzone(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-signzone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.signzone('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-signzone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unsignzone - errors', () => {
      it('should have a unsignzone function', (done) => {
        try {
          assert.equal(true, typeof a.unsignzone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.unsignzone(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-unsignzone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneName', (done) => {
        try {
          a.unsignzone('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'zoneName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-unsignzone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pushDNS - errors', () => {
      it('should have a pushDNS function', (done) => {
        try {
          assert.equal(true, typeof a.pushDNS === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.pushDNS(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-pushDNS', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.pushDNS('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-pushDNS', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.pushDNS('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-pushDNS', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDNSServer - errors', () => {
      it('should have a getDNSServer function', (done) => {
        try {
          assert.equal(true, typeof a.getDNSServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getDNSServer(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getDNSServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fqdn', (done) => {
        try {
          a.getDNSServer('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'fqdn is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getDNSServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.getDNSServer('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getDNSServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchDNSServer - errors', () => {
      it('should have a searchDNSServer function', (done) => {
        try {
          assert.equal(true, typeof a.searchDNSServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.searchDNSServer(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchDNSServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.searchDNSServer('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchDNSServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDNS - errors', () => {
      it('should have a createDNS function', (done) => {
        try {
          assert.equal(true, typeof a.createDNS === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.createDNS(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-createDNS', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.createDNS('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-createDNS', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDNSServer - errors', () => {
      it('should have a deleteDNSServer function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDNSServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.deleteDNSServer(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteDNSServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverFQDN', (done) => {
        try {
          a.deleteDNSServer('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serverFQDN is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteDNSServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyDNS - errors', () => {
      it('should have a modifyDNS function', (done) => {
        try {
          assert.equal(true, typeof a.modifyDNS === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.modifyDNS(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyDNS', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverFQDN', (done) => {
        try {
          a.modifyDNS('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serverFQDN is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyDNS', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.modifyDNS('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyDNS', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDNSServerZone - errors', () => {
      it('should have a getDNSServerZone function', (done) => {
        try {
          assert.equal(true, typeof a.getDNSServerZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getDNSServerZone(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getDNSServerZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fqdn', (done) => {
        try {
          a.getDNSServerZone('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fqdn is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getDNSServerZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.getDNSServerZone('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getDNSServerZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#managezone - errors', () => {
      it('should have a managezone function', (done) => {
        try {
          assert.equal(true, typeof a.managezone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.managezone(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-managezone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fqdn', (done) => {
        try {
          a.managezone('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'fqdn is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-managezone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.managezone('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-managezone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnsServerTemplate - errors', () => {
      it('should have a getDnsServerTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.getDnsServerTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.getDnsServerTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getDnsServerTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getDnsServerTemplate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getDnsServerTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getDnsServerTemplate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getDnsServerTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnsServerTemplate - errors', () => {
      it('should have a deleteDnsServerTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDnsServerTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.deleteDnsServerTemplate(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteDnsServerTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteDnsServerTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteDnsServerTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDNSServerTemplate - errors', () => {
      it('should have a createDNSServerTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createDNSServerTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.createDNSServerTemplate(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-createDNSServerTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.createDNSServerTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-createDNSServerTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyDnsServerTemplate - errors', () => {
      it('should have a modifyDnsServerTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.modifyDnsServerTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.modifyDnsServerTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyDnsServerTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.modifyDnsServerTemplate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyDnsServerTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.modifyDnsServerTemplate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyDnsServerTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchDnsServerTemplate - errors', () => {
      it('should have a searchDnsServerTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.searchDnsServerTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.searchDnsServerTemplate(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchDnsServerTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.searchDnsServerTemplate('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchDnsServerTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateDHCP - errors', () => {
      it('should have a generateDHCP function', (done) => {
        try {
          assert.equal(true, typeof a.generateDHCP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.generateDHCP(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-generateDHCP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.generateDHCP('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-generateDHCP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.generateDHCP('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-generateDHCP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createScope - errors', () => {
      it('should have a createScope function', (done) => {
        try {
          assert.equal(true, typeof a.createScope === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.createScope(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-createScope', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.createScope('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-createScope', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyScope - errors', () => {
      it('should have a modifyScope function', (done) => {
        try {
          assert.equal(true, typeof a.modifyScope === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.modifyScope(null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyScope', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startAddress', (done) => {
        try {
          a.modifyScope('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'startAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyScope', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endAddress', (done) => {
        try {
          a.modifyScope('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'endAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyScope', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.modifyScope('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyScope', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDHCP - errors', () => {
      it('should have a createDHCP function', (done) => {
        try {
          assert.equal(true, typeof a.createDHCP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.createDHCP(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-createDHCP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.createDHCP('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-createDHCP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDHCPServer - errors', () => {
      it('should have a deleteDHCPServer function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDHCPServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.deleteDHCPServer(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteDHCPServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverFQDN', (done) => {
        try {
          a.deleteDHCPServer('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serverFQDN is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteDHCPServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyDHCP - errors', () => {
      it('should have a modifyDHCP function', (done) => {
        try {
          assert.equal(true, typeof a.modifyDHCP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.modifyDHCP(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyDHCP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverFQDN', (done) => {
        try {
          a.modifyDHCP('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serverFQDN is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyDHCP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.modifyDHCP('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyDHCP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchDHCPServer - errors', () => {
      it('should have a searchDHCPServer function', (done) => {
        try {
          assert.equal(true, typeof a.searchDHCPServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.searchDHCPServer(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchDHCPServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.searchDHCPServer('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchDHCPServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDHCPServer - errors', () => {
      it('should have a getDHCPServer function', (done) => {
        try {
          assert.equal(true, typeof a.getDHCPServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getDHCPServer(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getDHCPServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fqdn', (done) => {
        try {
          a.getDHCPServer('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'fqdn is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getDHCPServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.getDHCPServer('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getDHCPServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#explicitBlock - errors', () => {
      it('should have a explicitBlock function', (done) => {
        try {
          assert.equal(true, typeof a.explicitBlock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.explicitBlock(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-explicitBlock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.explicitBlock('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-explicitBlock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#allocateBlock - errors', () => {
      it('should have a allocateBlock function', (done) => {
        try {
          assert.equal(true, typeof a.allocateBlock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.allocateBlock(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-allocateBlock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.allocateBlock('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-allocateBlock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#recoverBlock - errors', () => {
      it('should have a recoverBlock function', (done) => {
        try {
          assert.equal(true, typeof a.recoverBlock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.recoverBlock(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-recoverBlock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.recoverBlock('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-recoverBlock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#quickBlock - errors', () => {
      it('should have a quickBlock function', (done) => {
        try {
          assert.equal(true, typeof a.quickBlock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.quickBlock(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-quickBlock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.quickBlock('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-quickBlock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkStatus - errors', () => {
      it('should have a checkStatus function', (done) => {
        try {
          assert.equal(true, typeof a.checkStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.checkStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-checkStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing token', (done) => {
        try {
          a.checkStatus('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'token is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-checkStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.checkStatus('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-checkStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadConfigFiles - errors', () => {
      it('should have a downloadConfigFiles function', (done) => {
        try {
          assert.equal(true, typeof a.downloadConfigFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.downloadConfigFiles(null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-downloadConfigFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing token', (done) => {
        try {
          a.downloadConfigFiles('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'token is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-downloadConfigFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#decodeX509Certificate - errors', () => {
      it('should have a decodeX509Certificate function', (done) => {
        try {
          assert.equal(true, typeof a.decodeX509Certificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificate', (done) => {
        try {
          a.decodeX509Certificate(null, (data, error) => {
            try {
              const displayE = 'certificate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-decodeX509Certificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateKeyPair - errors', () => {
      it('should have a generateKeyPair function', (done) => {
        try {
          assert.equal(true, typeof a.generateKeyPair === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.generateKeyPair(null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-generateKeyPair', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSamlIdp - errors', () => {
      it('should have a addSamlIdp function', (done) => {
        try {
          assert.equal(true, typeof a.addSamlIdp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.addSamlIdp(null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-addSamlIdp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifySamlIdentityProvider - errors', () => {
      it('should have a modifySamlIdentityProvider function', (done) => {
        try {
          assert.equal(true, typeof a.modifySamlIdentityProvider === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.modifySamlIdentityProvider(null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifySamlIdentityProvider', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getByName - errors', () => {
      it('should have a getByName function', (done) => {
        try {
          assert.equal(true, typeof a.getByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.getByName(null, null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing samlIdpName', (done) => {
        try {
          a.getByName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'samlIdpName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchSamlIdpByName - errors', () => {
      it('should have a searchSamlIdpByName function', (done) => {
        try {
          assert.equal(true, typeof a.searchSamlIdpByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.searchSamlIdpByName(null, null, null, null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchSamlIdpByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing samlIdpName', (done) => {
        try {
          a.searchSamlIdpByName('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'samlIdpName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchSamlIdpByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSAMLIdentityProvider - errors', () => {
      it('should have a deleteSAMLIdentityProvider function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSAMLIdentityProvider === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing samlIdpName', (done) => {
        try {
          a.deleteSAMLIdentityProvider(null, (data, error) => {
            try {
              const displayE = 'samlIdpName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteSAMLIdentityProvider', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSamlConfiguration - errors', () => {
      it('should have a updateSamlConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.updateSamlConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.updateSamlConfiguration(null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-updateSamlConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSamlConfiguration - errors', () => {
      it('should have a getSamlConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getSamlConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.getSamlConfiguration(null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getSamlConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRunningSamlConfiguration - errors', () => {
      it('should have a getRunningSamlConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getRunningSamlConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.getRunningSamlConfiguration(null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getRunningSamlConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#qipSearch - errors', () => {
      it('should have a qipSearch function', (done) => {
        try {
          assert.equal(true, typeof a.qipSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.qipSearch(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-qipSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.qipSearch('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-qipSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV1OrgNameManagedTsigkeyExtension - errors', () => {
      it('should have a getV1OrgNameManagedTsigkeyExtension function', (done) => {
        try {
          assert.equal(true, typeof a.getV1OrgNameManagedTsigkeyExtension === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getV1OrgNameManagedTsigkeyExtension(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getV1OrgNameManagedTsigkeyExtension', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.getV1OrgNameManagedTsigkeyExtension('fakeparam', null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getV1OrgNameManagedTsigkeyExtension', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV1OrgNameManagedTsigkey - errors', () => {
      it('should have a postV1OrgNameManagedTsigkey function', (done) => {
        try {
          assert.equal(true, typeof a.postV1OrgNameManagedTsigkey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.postV1OrgNameManagedTsigkey(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-postV1OrgNameManagedTsigkey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV1OrgNameManagedTsigkey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-postV1OrgNameManagedTsigkey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdmin - errors', () => {
      it('should have a getAdmin function', (done) => {
        try {
          assert.equal(true, typeof a.getAdmin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getAdmin(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loginName', (done) => {
        try {
          a.getAdmin('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'loginName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.getAdmin('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchAdmin - errors', () => {
      it('should have a searchAdmin function', (done) => {
        try {
          assert.equal(true, typeof a.searchAdmin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.searchAdmin(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.searchAdmin('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAdmin - errors', () => {
      it('should have a createAdmin function', (done) => {
        try {
          assert.equal(true, typeof a.createAdmin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.createAdmin(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-createAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.createAdmin('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-createAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyAdmin - errors', () => {
      it('should have a modifyAdmin function', (done) => {
        try {
          assert.equal(true, typeof a.modifyAdmin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.modifyAdmin(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loginName', (done) => {
        try {
          a.modifyAdmin('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'loginName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.modifyAdmin('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAdmin - errors', () => {
      it('should have a deleteAdmin function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAdmin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.deleteAdmin(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loginName', (done) => {
        try {
          a.deleteAdmin('fakeparam', null, (data, error) => {
            try {
              const displayE = 'loginName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changePassword - errors', () => {
      it('should have a changePassword function', (done) => {
        try {
          assert.equal(true, typeof a.changePassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.changePassword(null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-changePassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loginName', (done) => {
        try {
          a.changePassword('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'loginName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-changePassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing newPassword', (done) => {
        try {
          a.changePassword('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'newPassword is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-changePassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing confirmNewPassword', (done) => {
        try {
          a.changePassword('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'confirmNewPassword is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-changePassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getManagedList - errors', () => {
      it('should have a getManagedList function', (done) => {
        try {
          assert.equal(true, typeof a.getManagedList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getManagedList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getManagedList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loginName', (done) => {
        try {
          a.getManagedList('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'loginName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getManagedList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing assignedOrgName', (done) => {
        try {
          a.getManagedList('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'assignedOrgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getManagedList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.getManagedList('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getManagedList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignManageList - errors', () => {
      it('should have a assignManageList function', (done) => {
        try {
          assert.equal(true, typeof a.assignManageList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.assignManageList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-assignManageList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loginName', (done) => {
        try {
          a.assignManageList('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'loginName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-assignManageList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing assignedOrgName', (done) => {
        try {
          a.assignManageList('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'assignedOrgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-assignManageList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.assignManageList('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-assignManageList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReport - errors', () => {
      it('should have a getReport function', (done) => {
        try {
          assert.equal(true, typeof a.getReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getReport(null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing adminStatus', (done) => {
        try {
          a.getReport('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'adminStatus is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportFormat', (done) => {
        try {
          a.getReport('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'reportFormat is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activityreport - errors', () => {
      it('should have a activityreport function', (done) => {
        try {
          assert.equal(true, typeof a.activityreport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.activityreport(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-activityreport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing administrator', (done) => {
        try {
          a.activityreport('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'administrator is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-activityreport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportFormat', (done) => {
        try {
          a.activityreport('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'reportFormat is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-activityreport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAdminRole - errors', () => {
      it('should have a createAdminRole function', (done) => {
        try {
          assert.equal(true, typeof a.createAdminRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.createAdminRole(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-createAdminRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.createAdminRole('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-createAdminRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchAdminRole - errors', () => {
      it('should have a searchAdminRole function', (done) => {
        try {
          assert.equal(true, typeof a.searchAdminRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.searchAdminRole(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchAdminRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.searchAdminRole('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchAdminRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyAdminRole - errors', () => {
      it('should have a modifyAdminRole function', (done) => {
        try {
          assert.equal(true, typeof a.modifyAdminRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.modifyAdminRole(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyAdminRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing adminRoleName', (done) => {
        try {
          a.modifyAdminRole('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'adminRoleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyAdminRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.modifyAdminRole('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyAdminRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAdminRole - errors', () => {
      it('should have a deleteAdminRole function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAdminRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.deleteAdminRole(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteAdminRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing adminRoleName', (done) => {
        try {
          a.deleteAdminRole('fakeparam', null, (data, error) => {
            try {
              const displayE = 'adminRoleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteAdminRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchAdminRoleAssignedAdmin - errors', () => {
      it('should have a searchAdminRoleAssignedAdmin function', (done) => {
        try {
          assert.equal(true, typeof a.searchAdminRoleAssignedAdmin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.searchAdminRoleAssignedAdmin(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchAdminRoleAssignedAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.searchAdminRoleAssignedAdmin('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchAdminRoleAssignedAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing adminRoleName', (done) => {
        try {
          a.searchAdminRoleAssignedAdmin('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'adminRoleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchAdminRoleAssignedAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdminRoleManagedList - errors', () => {
      it('should have a getAdminRoleManagedList function', (done) => {
        try {
          assert.equal(true, typeof a.getAdminRoleManagedList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getAdminRoleManagedList(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getAdminRoleManagedList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.getAdminRoleManagedList('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getAdminRoleManagedList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing adminRoleName', (done) => {
        try {
          a.getAdminRoleManagedList('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'adminRoleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getAdminRoleManagedList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignAdminRoleManagedList - errors', () => {
      it('should have a assignAdminRoleManagedList function', (done) => {
        try {
          assert.equal(true, typeof a.assignAdminRoleManagedList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.assignAdminRoleManagedList(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-assignAdminRoleManagedList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing adminRoleName', (done) => {
        try {
          a.assignAdminRoleManagedList('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'adminRoleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-assignAdminRoleManagedList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.assignAdminRoleManagedList('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-assignAdminRoleManagedList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDHCPTemplate - errors', () => {
      it('should have a createDHCPTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createDHCPTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.createDHCPTemplate(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-createDHCPTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.createDHCPTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-createDHCPTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyDHCPOptonTemplate - errors', () => {
      it('should have a modifyDHCPOptonTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.modifyDHCPOptonTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.modifyDHCPOptonTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyDHCPOptonTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.modifyDHCPOptonTemplate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyDHCPOptonTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.modifyDHCPOptonTemplate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyDHCPOptonTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDhcpOptionTemplate - errors', () => {
      it('should have a deleteDhcpOptionTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDhcpOptionTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.deleteDhcpOptionTemplate(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteDhcpOptionTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.deleteDhcpOptionTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteDhcpOptionTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDHCPOptionTemplate - errors', () => {
      it('should have a getDHCPOptionTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.getDHCPOptionTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getDHCPOptionTemplate(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getDHCPOptionTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.getDHCPOptionTemplate('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getDHCPOptionTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing infraType', (done) => {
        try {
          a.getDHCPOptionTemplate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'infraType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getDHCPOptionTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchDhcpOptionTemplateOptNum - errors', () => {
      it('should have a searchDhcpOptionTemplateOptNum function', (done) => {
        try {
          assert.equal(true, typeof a.searchDhcpOptionTemplateOptNum === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.searchDhcpOptionTemplateOptNum(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchDhcpOptionTemplateOptNum', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.searchDhcpOptionTemplateOptNum('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchDhcpOptionTemplateOptNum', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing optionNumber', (done) => {
        try {
          a.searchDhcpOptionTemplateOptNum('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'optionNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchDhcpOptionTemplateOptNum', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchDhcpOptionTemplateName - errors', () => {
      it('should have a searchDhcpOptionTemplateName function', (done) => {
        try {
          assert.equal(true, typeof a.searchDhcpOptionTemplateName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.searchDhcpOptionTemplateName(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchDhcpOptionTemplateName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.searchDhcpOptionTemplateName('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchDhcpOptionTemplateName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.searchDhcpOptionTemplateName('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchDhcpOptionTemplateName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpPolicyTemplate - errors', () => {
      it('should have a getDhcpPolicyTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.getDhcpPolicyTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getDhcpPolicyTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getDhcpPolicyTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.getDhcpPolicyTemplate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getDhcpPolicyTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyTemplateName', (done) => {
        try {
          a.getDhcpPolicyTemplate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyTemplateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getDhcpPolicyTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDhcpPolicyTemplate - errors', () => {
      it('should have a createDhcpPolicyTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createDhcpPolicyTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.createDhcpPolicyTemplate(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-createDhcpPolicyTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.createDhcpPolicyTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-createDhcpPolicyTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyDHCPolicyTemplate - errors', () => {
      it('should have a modifyDHCPolicyTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.modifyDHCPolicyTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.modifyDHCPolicyTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyDHCPolicyTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.modifyDHCPolicyTemplate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyDHCPolicyTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.modifyDHCPolicyTemplate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyDHCPolicyTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDHCPPolicyTemplate - errors', () => {
      it('should have a deleteDHCPPolicyTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDHCPPolicyTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.deleteDHCPPolicyTemplate(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteDHCPPolicyTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.deleteDHCPPolicyTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteDHCPPolicyTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV1OrgNameNode - errors', () => {
      it('should have a postV1OrgNameNode function', (done) => {
        try {
          assert.equal(true, typeof a.postV1OrgNameNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.postV1OrgNameNode(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-postV1OrgNameNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.postV1OrgNameNode('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-postV1OrgNameNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV1OrgNameNode - errors', () => {
      it('should have a putV1OrgNameNode function', (done) => {
        try {
          assert.equal(true, typeof a.putV1OrgNameNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.putV1OrgNameNode(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-putV1OrgNameNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uniqueID', (done) => {
        try {
          a.putV1OrgNameNode('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'uniqueID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-putV1OrgNameNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.putV1OrgNameNode('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-putV1OrgNameNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV1OrgNameNodeExtension - errors', () => {
      it('should have a getV1OrgNameNodeExtension function', (done) => {
        try {
          assert.equal(true, typeof a.getV1OrgNameNodeExtension === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getV1OrgNameNodeExtension(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getV1OrgNameNodeExtension', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.getV1OrgNameNodeExtension('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getV1OrgNameNodeExtension', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV1OrgNameNodeOptionsExtension - errors', () => {
      it('should have a getV1OrgNameNodeOptionsExtension function', (done) => {
        try {
          assert.equal(true, typeof a.getV1OrgNameNodeOptionsExtension === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getV1OrgNameNodeOptionsExtension(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getV1OrgNameNodeOptionsExtension', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.getV1OrgNameNodeOptionsExtension('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getV1OrgNameNodeOptionsExtension', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing options', (done) => {
        try {
          a.getV1OrgNameNodeOptionsExtension('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'options is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getV1OrgNameNodeOptionsExtension', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeInfo', (done) => {
        try {
          a.getV1OrgNameNodeOptionsExtension('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getV1OrgNameNodeOptionsExtension', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV1OrgNameNodeUniqueID - errors', () => {
      it('should have a deleteV1OrgNameNodeUniqueID function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV1OrgNameNodeUniqueID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.deleteV1OrgNameNodeUniqueID(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteV1OrgNameNodeUniqueID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uniqueID', (done) => {
        try {
          a.deleteV1OrgNameNodeUniqueID('fakeparam', null, (data, error) => {
            try {
              const displayE = 'uniqueID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteV1OrgNameNodeUniqueID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV1OrgNameNodeLinknodetoipaddresses - errors', () => {
      it('should have a postV1OrgNameNodeLinknodetoipaddresses function', (done) => {
        try {
          assert.equal(true, typeof a.postV1OrgNameNodeLinknodetoipaddresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.postV1OrgNameNodeLinknodetoipaddresses(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-postV1OrgNameNodeLinknodetoipaddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.postV1OrgNameNodeLinknodetoipaddresses('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-postV1OrgNameNodeLinknodetoipaddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV1OrgNameNodeUnlinkipv4addressesfromnodes - errors', () => {
      it('should have a postV1OrgNameNodeUnlinkipv4addressesfromnodes function', (done) => {
        try {
          assert.equal(true, typeof a.postV1OrgNameNodeUnlinkipv4addressesfromnodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.postV1OrgNameNodeUnlinkipv4addressesfromnodes(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-postV1OrgNameNodeUnlinkipv4addressesfromnodes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.postV1OrgNameNodeUnlinkipv4addressesfromnodes('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-postV1OrgNameNodeUnlinkipv4addressesfromnodes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSubnetOrganization - errors', () => {
      it('should have a addSubnetOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.addSubnetOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.addSubnetOrganization(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-addSubnetOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.addSubnetOrganization('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-addSubnetOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSubnetOrg - errors', () => {
      it('should have a getSubnetOrg function', (done) => {
        try {
          assert.equal(true, typeof a.getSubnetOrg === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getSubnetOrg(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getSubnetOrg', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.getSubnetOrg('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getSubnetOrg', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetOrgName', (done) => {
        try {
          a.getSubnetOrg('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'subnetOrgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getSubnetOrg', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pageSearch - errors', () => {
      it('should have a pageSearch function', (done) => {
        try {
          assert.equal(true, typeof a.pageSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.pageSearch(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-pageSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.pageSearch('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-pageSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifySubnetOrg - errors', () => {
      it('should have a modifySubnetOrg function', (done) => {
        try {
          assert.equal(true, typeof a.modifySubnetOrg === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.modifySubnetOrg(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifySubnetOrg', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetOrgName', (done) => {
        try {
          a.modifySubnetOrg('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'subnetOrgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifySubnetOrg', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.modifySubnetOrg('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifySubnetOrg', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSubnetOrg - errors', () => {
      it('should have a deleteSubnetOrg function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSubnetOrg === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.deleteSubnetOrg(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteSubnetOrg', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetOrgName', (done) => {
        try {
          a.deleteSubnetOrg('fakeparam', null, (data, error) => {
            try {
              const displayE = 'subnetOrgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteSubnetOrg', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDHCPAndV4SubnetMacPool - errors', () => {
      it('should have a createDHCPAndV4SubnetMacPool function', (done) => {
        try {
          assert.equal(true, typeof a.createDHCPAndV4SubnetMacPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.createDHCPAndV4SubnetMacPool(null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-createDHCPAndV4SubnetMacPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing infraValue', (done) => {
        try {
          a.createDHCPAndV4SubnetMacPool('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'infraValue is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-createDHCPAndV4SubnetMacPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing infraType', (done) => {
        try {
          a.createDHCPAndV4SubnetMacPool('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'infraType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-createDHCPAndV4SubnetMacPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.createDHCPAndV4SubnetMacPool('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-createDHCPAndV4SubnetMacPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyDHCPAndV4SubnetMacPool - errors', () => {
      it('should have a modifyDHCPAndV4SubnetMacPool function', (done) => {
        try {
          assert.equal(true, typeof a.modifyDHCPAndV4SubnetMacPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.modifyDHCPAndV4SubnetMacPool(null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyDHCPAndV4SubnetMacPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing infraValue', (done) => {
        try {
          a.modifyDHCPAndV4SubnetMacPool('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'infraValue is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyDHCPAndV4SubnetMacPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing infraType', (done) => {
        try {
          a.modifyDHCPAndV4SubnetMacPool('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'infraType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyDHCPAndV4SubnetMacPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.modifyDHCPAndV4SubnetMacPool('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyDHCPAndV4SubnetMacPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDHCPAndV4SubnetMacPool - errors', () => {
      it('should have a deleteDHCPAndV4SubnetMacPool function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDHCPAndV4SubnetMacPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.deleteDHCPAndV4SubnetMacPool(null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteDHCPAndV4SubnetMacPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing infraValue', (done) => {
        try {
          a.deleteDHCPAndV4SubnetMacPool('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'infraValue is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteDHCPAndV4SubnetMacPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing infraType', (done) => {
        try {
          a.deleteDHCPAndV4SubnetMacPool('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'infraType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteDHCPAndV4SubnetMacPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.deleteDHCPAndV4SubnetMacPool('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteDHCPAndV4SubnetMacPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDHCPAndV4SubnetMacPool - errors', () => {
      it('should have a getDHCPAndV4SubnetMacPool function', (done) => {
        try {
          assert.equal(true, typeof a.getDHCPAndV4SubnetMacPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getDHCPAndV4SubnetMacPool(null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getDHCPAndV4SubnetMacPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.getDHCPAndV4SubnetMacPool('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getDHCPAndV4SubnetMacPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing infraValue', (done) => {
        try {
          a.getDHCPAndV4SubnetMacPool('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'infraValue is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getDHCPAndV4SubnetMacPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing infraType', (done) => {
        try {
          a.getDHCPAndV4SubnetMacPool('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'infraType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getDHCPAndV4SubnetMacPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addACLTemplate - errors', () => {
      it('should have a addACLTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.addACLTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.addACLTemplate(null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-addACLTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.addACLTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-addACLTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getACLTemplate - errors', () => {
      it('should have a getACLTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.getACLTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getACLTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getACLTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing aclTemplateName', (done) => {
        try {
          a.getACLTemplate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'aclTemplateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getACLTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.getACLTemplate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-getACLTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyAclTemplate - errors', () => {
      it('should have a modifyAclTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.modifyAclTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.modifyAclTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyAclTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing aclTemplateName', (done) => {
        try {
          a.modifyAclTemplate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'aclTemplateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyAclTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.modifyAclTemplate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-modifyAclTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAclTemplate - errors', () => {
      it('should have a deleteAclTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAclTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.deleteAclTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteAclTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing aclTemplateName', (done) => {
        try {
          a.deleteAclTemplate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'aclTemplateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteAclTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing force', (done) => {
        try {
          a.deleteAclTemplate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'force is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-deleteAclTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchACLTemplate - errors', () => {
      it('should have a searchACLTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.searchACLTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.searchACLTemplate(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchACLTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extension', (done) => {
        try {
          a.searchACLTemplate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'extension is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_vitalqip-adapter-searchACLTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
